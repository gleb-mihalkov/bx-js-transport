!(function($) {

  if ($ == null) {
    console.error('jQuery is required');
    return;
  }

  /**
   * Имя переменной со значениями из PHP.
   * @type {String}
   */
  var transportVar = '_phpTransportValues';

  /**
   * Имя заголовка со значениями из PHP.
   * @type {String}
   */
  var transportHeader = 'X-PHP-Transport';

  /**
   * Простанство имен событий.
   * @type {String}
   */
  var eventNamespace = 'phpTransport';

  /**
   * Обрабатывает значения, пришедшие через транспорт.
   * @param  {[type]} values Набор значений из транспорта.
   * @return {void}
   */
  function processValues(values) {
    for (var key in values) {
      if (!values.hasOwnProperty(key)) {
        continue;
      }

      var value = values[key];

      var eventName = key + '.' + eventNamespace;
      var event = new $.Event(eventName);
      event.value = value;

      $(window).trigger(event);
    }
  }

  /**
   * Обрабатывает событие окончания асинхронного запроса.
   * @param  {Event} e   Событие.
   * @param  {jqXHR} xhr Объект асинхронного запроса.
   * @return {void}
   */
  function onRequest(e, xhr) {
    var header = xhr.getResponseHeader(transportHeader);
    header = header ? header.replace(/\+/g, '%20') : null;

    var json = header ? decodeURIComponent(header) : null;
    var values = json ? $.parseJSON(json) : null;

    if (values == null) {
      return;
    }

    processValues(values);
  }

  /**
   * Обрабатывает событие окончания загрузки страницы.
   * @return {void}
   */
  function onReady() {
    var values = window[transportVar];

    if (values == null) {
      return;
    }

    processValues(values);
  }

  // Прикрепляем события.
  $(document)
    .ajaxComplete(onRequest)
    .ready(onReady);

})(window.jQuery);

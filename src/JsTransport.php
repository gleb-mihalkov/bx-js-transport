<?php
namespace Bx\JsTransport;
use Bx\Service\Service;
use Bx\Assert\Assert;
use Bitrix\Main\Page\Asset;

/**
 * Транспорт сообщений от PHP для клиентского Javascript.
 */
class JsTransport extends Service
{
    /**
     * Название заголовка для транспорта значений.
     * @var string
     */
    const HEADER_NAME = 'X-PHP-Transport';

    /**
     * Название переменной для транспорта значений.
     * @var string
     */
    const VAR_NAME = '_phpTransportValues';



    /**
     * Коллекция значений для передачи клиентскому Javascript.
     * @var array
     */
    protected $values = [];

    /**
     * Задает значение для передачи клиентскому Javascript.
     * @param string $key   Уникальный ключ.
     * @param mixed  $value Значение.
     */
    protected function set(string $key, $value)
    {
        Assert::notEmpty($key);
        $this->values[$key] = $value;
    }

    /**
     * Возвращает значение для передачи клиентскому Javascript по его уникальному ключу.
     * @param  string $key Уникальный ключ.
     * @return mixed       Значение.
     */
    protected function get(string $key)
    {
        Assert::notEmpty($key);
        return $this->values[$key] ?? null;
    }



    /**
     * Добавляет тег SCRIPT в секцию HEAD страницы.
     * @param string $content Содержимое тега.
     */
    private function addScriptAsset(string $content)
    {
        $content = trim($content);

        if (empty($content))
        {
            return;
        }

        $script = '<script type="text/javascript">'.$content.'</script>';

        $asset = Asset::getInstance();
        $asset->addString($script);
    }

    /**
     * Возвращает JS-код обработчика сообщений от транспорта.
     * @return string JS-код обработчика.
     */
    private function getHandlerJavascript()
    {
        $fileRaw = __DIR__.'/JsTransportHandler.js';
        $fileMin = __DIR__.'/JsTransportHandler.min.js';

        return file_exists($fileMin)
            ? file_get_contents($fileMin)
            : file_get_contents($fileRaw);
    }

    /**
     * Добавляет JS-код обработчика сообщений от транспорта в список скриптов страницы.
     */
    private function addHandlerJavascript()
    {
        $text = $this->getHandlerJavascript();
        $this->addScriptAsset($text);
    }

    /**
     * Возвращает JS-код значений для транспорта.
     * @return string JS-код значений.
     */
    private function getValuesJavascript() : string
    {
        $json = json_encode($this->values);
        return $json;
    }

    /**
     * Добавляет JS-код значений для транспорта в заголовки ответа.
     */
    private function addHeaderValuesJavascript()
    {
        $json = $this->getValuesJavascript();
        $json = urlencode($json);

        header(self::HEADER_NAME.': '.$json);
    }

    /**
     * Добавляет JS-код значений для транспорта в тело ответа.
     */
    private function addBodyValuesJavascript()
    {
        $json = $this->getValuesJavascript();

        $script = 'window.'.self::VAR_NAME.' = '.$json.';';
        $this->addScriptAsset($script);
    }



    /**
     * Возвращает true, если данный скрипт был вызван с помощью HTTP запроса из браузера, иначе
     * возвращает false.
     * @return boolean True или false.
     */
    protected function isRequest()
    {
        return php_sapi_name() !== 'cli';
    }

    /**
     * Возвращает true, если данных скрипт был вызыван по синхронным HTTP-запросом и должен
     * отдавать HTML-страницу, иначе возвращает false.
     * @return boolean True или false.
     */
    protected function isHtmlRequest()
    {
        $accept = $_SERVER['HTTP_ACCEPT'] ?: '';
        $accept = strtolower($accept);
        return strpos($accept, 'text/html') !== false;
    }

    /**
     * Возвращает true, если запрос асинхронный, в противном случае возвращает false.
     * @return boolean True или false.
     */
    protected function isAjaxRequest()
    {
        return strtolower($_SERVER['HTTP_X_REQUESTED_WITH'] ?: '') === 'xmlhttprequest';
    }



    /**
     * Инициализирует сервис.
     * @return void
     */
    protected function boot()
    {
        if (!$this->isRequest())
        {
            return;
        }

        $this->bind('OnEpilog', 'onFinish');
    }

    /**
     * Обрабатывает событие загрузки страницы.
     * @return void
     */
    protected function onFinish()
    {
        if ($this->isAjaxRequest())
        {
            $this->addHeaderValuesJavascript();
            return;
        }

        if (!$this->isHtmlRequest())
        {
            return;
        }

        $this->addBodyValuesJavascript();
        $this->addHandlerJavascript();
    }
}
